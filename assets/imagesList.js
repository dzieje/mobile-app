export const logo = require('./images/logo.png');
export const ministryLogo = require('./images/ministry_of_culture_logo.jpg');
export const defaultProfileImage = require('./images/default_profile.png');

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import store from './store/index';

import {Provider} from 'react-redux';
import Navigation from './navigation/Navigation';
import {
  ActionSheetProvider,
  connectActionSheet,
} from '@expo/react-native-action-sheet';
import AlertView from './components/AlertView';
import LoaderView from './components/LoaderView';
require('./http/http');
function App() {
  return (
    <Provider store={store}>
      <Navigation></Navigation>
      <LoaderView show={true}></LoaderView>
      <AlertView></AlertView>
    </Provider>
  );
}

export default App;

import { Dimensions, Platform, StatusBar } from "react-native";

export const refreshTokenName = "DziejeRefreshToken";
export const URL = "https://dzieje.ebusters.com.pl/";
export const clientID = 1;
export const clientSecret = "6rJJmbcQ0n0v2U30QvydjAAIfAyLN1W0YZ6ggmvY";

//Colors
export const purpleColor = "#535FAB";
export const redColor = "#E2533E";
export const greenColor = "#35a978";
export const greyColor = "#7F8789";
export const greyDarkColor = "#A8ADB3";
export const greyLightColor = "#E5E6E8";
export const blueColor = "#36B9CC";
export const darkBlueColor = "#2A96A5";
export const yellowColor = "#e8d900";

export const labelColor = "#FFF";
export const inputBackgroundColor = "#FFF";

//Sizes
export const borderRadius = 15;
export const deviceWidth = Dimensions.get("window").width;
export const deviceHeight = Dimensions.get("window").height;

export const inputHeight = 50;
export const buttonHeight = 50;
export const buttonWidth = deviceWidth * 0.5;
export const buttonCornerRadius = 28;
export const componentHeight = 50;
export const headerHeight = 40;
export const tabBarHeight = 50;
export const animationTimeMs = 500;
export const playerHeight = 120;

export const childsWidth = "95%";
export const borderWidth = 1;
export const marginBetweenComponents = 10;
export const marginFromTitle = 20;
export const marginBetweenSections = 20;
export const marginBottom = 20;
export const smallFontSize = 12;
export const contentFontSize = 16;
export const headerFontSize = 18;
export const inputLabelFontSize = 12;
export function getSafeAreaBottom() {
  if (Platform.OS === "ios") {
    if (deviceHeight > 800) {
      return 20;
    } else {
      return 0;
    }
  } else {
    return 0;
  }
}
export function getStatusBarHeight() {
  if (Platform.OS === "ios") {
    if (deviceHeight > 800) {
      return 44;
    } else {
      return 20;
    }
  } else {
    return StatusBar.currentHeight;
  }
}

export const minContentHeight =
  deviceHeight - getSafeAreaBottom() - tabBarHeight - getStatusBarHeight();

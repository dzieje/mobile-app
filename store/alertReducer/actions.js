export const SET_ALERT = 'SET_ALERT';
export function setAlert(alert) {
  return {
    type: SET_ALERT,
    alert: alert,
  };
}

export const SET_MESSAGE = 'SET_MESSAGE';
export function setMessage(message) {
  return {
    type: SET_MESSAGE,
    message: message,
  };
}
export const SHOW_ALERT = 'SHOW_ALERT';
export function showAlert(shouldShow) {
  return {
    type: SHOW_ALERT,
    show: shouldShow,
  };
}

export const DISABLE_ALERT = 'DISABLE_ALERT';
export function disableAlert(value) {
  return {
    type: DISABLE_ALERT,
    disable: value,
  };
}

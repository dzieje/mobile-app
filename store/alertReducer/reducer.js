import {SET_ALERT, SET_MESSAGE, SHOW_ALERT, DISABLE_ALERT} from './actions';
const INITIAL_STATE = {
  backgroundColor: '#FEDA00',
  message: '',
  displayTimeMs: 1000,
  show: false,
  disable: false,
};

const alertReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_ALERT:
      return Object.assign({}, state, {
        backgroundColor: action.alert.backgroundColor,
        message: action.alert.message,
        show: action.alert.show,
      });
    case SET_MESSAGE:
      return Object.assign({}, state, {
        message: action.message,
      });
    case SHOW_ALERT:
      return Object.assign({}, state, {
        show: action.show,
      });
    case DISABLE_ALERT:
      return Object.assign({}, state, {
        disable: action.disable,
      });
    default:
      return state;
  }
};

export default alertReducer;

import {
  SET_ACCESS_TOKEN,
  SET_REFRESH_TOKEN,
  SET_LOGIN_REQUEST_REPEATED,
} from "./actions";

const INITIAL_STATE = {
  accessToken: "",
  refreshToken: "",
  loginRequestRepeated: false,
};

const sessionReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_ACCESS_TOKEN:
      return Object.assign({}, state, { accessToken: action.token });
    case SET_REFRESH_TOKEN:
      return Object.assign({}, state, { refreshToken: action.token });
    case SET_LOGIN_REQUEST_REPEATED:
      return Object.assign({}, state, {
        loginRequestRepeated: action.requestRepeated,
      });
    default:
      return state;
  }
};

export default sessionReducer;

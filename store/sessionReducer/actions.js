import axios from "axios";
import * as AppConstants from "../../constants";
import * as SecureStore from "expo-secure-store";
import { setLoggedIn } from "../appReducer/actions";

export const SET_ACCESS_TOKEN = "SET_ACCESS_TOKEN";
export function setAccessToken(token) {
  return {
    type: SET_ACCESS_TOKEN,
    token: token,
  };
}

export const SET_REFRESH_TOKEN = "SET_REFRESH_TOKEN";
export function setRefreshToken(token) {
  return {
    type: SET_REFRESH_TOKEN,
    token: token,
  };
}

export const SET_LOGIN_REQUEST_REPEATED = "SET_LOGIN_REQUEST_REPEATED";
export function setLoginRequestRepated(value) {
  return {
    type: SET_LOGIN_REQUEST_REPEATED,
    requestRepeated: value,
  };
}

export function login(data) {
  return function (dispatch) {
    return new Promise((resolve, reject) => {
      axios
        .post("oauth/token", {
          grant_type: "password",
          client_id: AppConstants.clientID,
          client_secret: AppConstants.clientSecret,
          username: data.login,
          password: data.password,
          scope: "*",
        })
        .then(function (response) {
          dispatch(setAccessToken(response.data.access_token));
          dispatch(setRefreshToken(response.data.refresh_token));
          SecureStore.setItemAsync(
            AppConstants.refreshTokenName,
            response.data.refresh_token
          );
          resolve(response);
        })
        .catch(function (error) {
          reject(error.response.data);
        });
    });
  };
}
export function refreshToken(token) {
  return function (dispatch) {
    return new Promise((resolve, reject) => {
      axios
        .post("oauth/token", {
          grant_type: "refresh_token",
          refresh_token: token,
          client_id: AppConstants.clientID,
          client_secret: AppConstants.clientSecret,
          scope: "*",
        })
        .then(function (response) {
          resolve(response);
          dispatch(setAccessToken(response.data.access_token));
          SecureStore.setItemAsync(
            AppConstants.refreshTokenName,
            response.data.refresh_token
          );
        })
        .catch(function (error) {
          if (error.response) {
            reject(error.response.data);
          } else {
            reject(error);
          }
        });
    });
  };
}

export function clearStore() {
  return {
    type: "CLEAR_STORE",
    value: "",
  };
}
export function logout() {
  return function (dispatch) {
    return new Promise((resolve, reject) => {
      axios
        .post("api/user/logout", {})
        .then(function (response) {
          resolve(response);
          dispatch(setAccessToken(""));
          SecureStore.setItemAsync(AppConstants.refreshTokenName, "");
          dispatch(clearStore());
          dispatch(setLoggedIn(false));
        })
        .catch(function (error) {
          dispatch(setAccessToken(""));
          SecureStore.setItemAsync(AppConstants.refreshTokenName, "");
          dispatch(clearStore());
          dispatch(setLoggedIn(false));
          if (error.response) {
            reject(error.response.data);
          } else {
            reject(error);
          }
        });
    });
  };
}

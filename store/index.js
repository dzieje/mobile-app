import {createStore, combineReducers, applyMiddleware} from 'redux';
import promise from 'redux-promise-middleware';
import thunk from 'redux-thunk';
import alertReducer from './alertReducer/reducer';
import appReducer from './appReducer/reducer';
import notificationReducer from './notificationReducer/reducer';
import sessionReducer from './sessionReducer/reducer';

const reducers = combineReducers({
  appReducer: appReducer,
  alertReducer: alertReducer,
  notificationReducer: notificationReducer,
  sessionReducer: sessionReducer,
});
const middleware = applyMiddleware(promise, thunk);
export default createStore(reducers, middleware);

import {SET_LOADER, SET_LOADER_REF, SET_LOGGED_IN, SET_USER_INFO} from './actions';

const INITIAL_STATE = {
  loggedIn: false,
  loader: false,
  loaderRef: null,
  profileData:{avatar_filename: "",
    date_of_birth: "",
    email: "",
    gender: "",
    login: "",
    name: "",
    phone: "",
    surname: ""}
};

const appReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_LOGGED_IN:
      return Object.assign({}, state, {loggedIn: action.logged});
    case SET_LOADER:
      return Object.assign({}, state, {
        loader: action.value,
      });
    case SET_LOADER_REF:
      return Object.assign({}, state, {
        loaderRef: action.ref,
      });
      case SET_USER_INFO:
      return Object.assign({}, state, {
        profileData: {
          avatar_filename: action.user.avatar_filename ? action.user.avatar_filename : "",
          date_of_birth: action.user.date_of_birth ? action.user.date_of_birth : "",
          email: action.user.email ? action.user.email : "",
          gender: action.user.gender ? action.user.gender : "",
          login: action.user.login ? action.user.login : "",
          name: action.user.name ? action.user.name : "",
          phone: action.user.phone ? action.user.phone : "",
          surname: action.user.surname ? action.user.surname : "",
      },
      });
    default:
      return state;
  }
};

export default appReducer;

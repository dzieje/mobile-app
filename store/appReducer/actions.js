import store from '../../store/index';
import axios from 'axios';

export const SET_LOGGED_IN = 'SET_LOGGED_IN';
export function setLoggedIn(value) {
  return {
    type: SET_LOGGED_IN,
    logged: value,
  };
}
export const SET_LOADER = 'SET_LOADER';
export function setLoader(isVisible) {
  return {
    type: SET_LOADER,
    value: isVisible,
  };
}
export const SET_USER_INFO = 'SET_USER_INFO';
export function setUserInfo(userData) {
  return {
    type: SET_USER_INFO,
    user: userData,
  };
}

export function showLoader(shouldShow) {
  let loaderRef = store.getState().appReducer.loaderRef;
  if (loaderRef != null) {
    if (shouldShow) {
      loaderRef.showLoader();
    } else {
      loaderRef.hideLoader();
    }
  }
  return {
    type: SET_LOADER,
    value: true,
  };
}

export const SET_LOADER_REF = 'SET_LOADER_REF';
export function setLoaderRef(ref) {
  return {
    type: SET_LOADER_REF,
    ref: ref,
  };
}

export function getUserInfo() {
  return function (dispatch) {
    return new Promise((resolve, reject) => {
      var config = {};
      config.params = {};
      axios
        .get('api/user/info', config)
        .then(function (response) {
          dispatch(setUserInfo(response.data));
          resolve(response.data);
        })
        .catch(function (error) {
          reject(error.response.data);
        });
    });
  };
}


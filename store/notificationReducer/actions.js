import messaging from "@react-native-firebase/messaging";
import axios from "axios";
import navigationService from "../../navigation/navigationService";
import store from "../index";
import PushNotificationIOS from "@react-native-community/push-notification-ios";
var PushNotification = require("react-native-push-notification");

export const SET_NOTIFICATIONS = "SET_NOTIFICATIONS";
function setNotifications(notifications) {
  return {
    type: SET_NOTIFICATIONS,
    notifications: notifications,
  };
}

export const ADD_NOTIFICATIONS = "ADD_NOTIFICATIONS";
function addNotifications(notifications) {
  return {
    type: ADD_NOTIFICATIONS,
    notifications: notifications,
  };
}

export const SET_TASKS = "SET_TASKS";
function setTasks(tasks) {
  return {
    type: SET_TASKS,
    tasks: tasks,
  };
}

export const ADD_TASKS = "ADD_TASKS";
function addTasks(tasks) {
  return {
    type: ADD_TASKS,
    tasks: tasks,
  };
}

export const ADD_TASK = "ADD_TASK";
function addTask(task) {
  return {
    type: ADD_TASK,
    task: task,
  };
}

export const SET_FCM_TOKEN = "SET_FCM_TOKEN";
function setToken(token) {
  return {
    type: SET_FCM_TOKEN,
    token: token,
  };
}

export const SET_BADGE_COUNT = "SET_BADGE_COUNT";
export function setBadge(badge) {
  PushNotification.setApplicationIconBadgeNumber(badge);
  return {
    type: SET_BADGE_COUNT,
    badge: badge,
  };
}
export const SET_TASKS_BADGE = "SET_TASKS_BADGE";
export function setTaskBadge(badge) {
  return {
    type: SET_TASKS_BADGE,
    badge: badge,
  };
}

export const SET_NEWS_BADGE = "SET_NEWS_BADGE";
export function setNewsBadge(badge) {
  return {
    type: SET_NEWS_BADGE,
    badge: badge,
  };
}

export const ADD_NOTIFICATION = "ADD_NOTIFICATION";
function addNotification(notification) {
  return {
    type: ADD_NOTIFICATION,
    notification: notification,
  };
}
export function registerAppWithFCM() {
  return function (dispatch) {
    messaging()
      .hasPermission()
      .then(() => {
        messaging()
          .getToken()
          .then((token) => {
            dispatch(sendFcmToken(token));
          });
      });
  };
}

export const SET_READED_NOTIFICATION = "SET_READED_NOTIFICATION";
export function setReadedMessage(id) {
  return {
    type: SET_READED_NOTIFICATION,
    id: id,
  };
}

export const SET_READED_TASKS = "SET_READED_TASKS";
export function setReadedTasks(id) {
  return {
    type: SET_READED_TASKS,
    id: id,
  };
}

export function sendFcmToken(token) {
  return function (dispatch) {
    return new Promise((resolve, reject) => {
      axios
        .post(
          "api/user/firebase-uuid",
          {
            firebase_uuid: token,
          },
          {}
        )
        .then(function (response) {
          dispatch(setToken(token));
          resolve(response.data);
        })
        .catch(function (error) {
          reject(error.response.data);
        });
    });
  };
}
export const SET_UNSUBSCRIBE_METHOD = "SET_UNSUBSCRIBE_METHOD";
function setUnsubscribeMethod(method) {
  return {
    type: SET_UNSUBSCRIBE_METHOD,
    method: method,
  };
}

export function getNotifications(page) {
  return function (dispatch) {
    return new Promise((resolve, reject) => {
      var config = {};
      config.params = {
        page: page,
      };
      axios
        .get("api/news", config)
        .then(function (response) {
          if (page == 1) {
            dispatch(setNotifications(response.data.data));
          } else {
            dispatch(addNotifications(response.data.data));
          }
          resolve(response.data);
        })
        .catch(function (error) {
          reject(error.response.data);
        });
    });
  };
}

export function getTasks(page) {
  return function (dispatch) {
    return new Promise((resolve, reject) => {
      var config = {};
      config.params = {
        page: page,
      };
      axios
        .get("api/tasks", config)
        .then(function (response) {
          if (page == 1) {
            dispatch(setTasks(response.data.data));
          } else {
            dispatch(addTasks(response.data.data));
          }
          resolve(response.data);
        })
        .catch(function (error) {
          reject(error.response.data);
        });
    });
  };
}

export function getNotificationDetails(id) {
  return function (dispatch) {
    return new Promise((resolve, reject) => {
      var config = {};
      axios
        .get(`api/news/show/${id}`, config)
        .then(function (response) {
          resolve(response.data);
        })
        .catch(function (error) {
          reject(error.response.data);
        });
    });
  };
}

export function getTasksDetails(id) {
  return function (dispatch) {
    return new Promise((resolve, reject) => {
      var config = {};
      axios
        .get(`api/tasks/show/${id}`, config)
        .then(function (response) {
          resolve(response.data);
        })
        .catch(function (error) {
          reject(error.response.data);
        });
    });
  };
}

export function registerByToken() {
  return function (dispatch) {
    if (store.getState().notificationReducer.unsubscribeMethod == undefined) {
      const unsubscribe = messaging().onMessage(async (remoteMessage) => {
        dispatch(setBadge(store.getState().notificationReducer.badge + 1));
        remoteMessage.data.unread = true;
        if (remoteMessage.data.type == "task") {
          dispatch(addTask(remoteMessage.data));
          dispatch(
            setTaskBadge(store.getState().notificationReducer.tasksBadge + 1)
          );
        } else {
          dispatch(addNotification(remoteMessage.data));
          dispatch(
            setNewsBadge(store.getState().notificationReducer.newsBadge + 1)
          );
        }
      });
      dispatch(setUnsubscribeMethod(unsubscribe));
    }
    messaging().onNotificationOpenedApp((remoteMessage) => {
      dispatch(checkNotification(remoteMessage));
    });
  };
}

export function checkNotification(remoteMessage) {
  return function (dispatch) {
    if (remoteMessage.data.type == "task") {
      navigationService.navigate("Tasks");
      dispatch(getTasksDetails(remoteMessage.data.id)).then((resp) => {
        navigationService.navigate("TasksDetails", {
          notification: remoteMessage.data,
          type: "task",
        });
      });
    } else {
      navigationService.navigate("Home");
      dispatch(getNotificationDetails(remoteMessage.data.id)).then((resp) => {
        navigationService.navigate("NewsDetails", {
          notification: remoteMessage.data,
          type: "notification",
        });
      });
    }
  };
}

import {
  ADD_NOTIFICATIONS,
  SET_FCM_TOKEN,
  SET_NOTIFICATIONS,
  SET_UNSUBSCRIBE_METHOD,
  SET_READED_NOTIFICATION,
  ADD_NOTIFICATION,
  SET_BADGE_COUNT,
  SET_TASKS,
  ADD_TASKS,
  SET_READED_TASKS,
  SET_TASKS_BADGE,
  SET_NEWS_BADGE,
  ADD_TASK,
} from "./actions";
import messaging from "@react-native-firebase/messaging";
import PushNotificationIOS from "@react-native-community/push-notification-ios";
var PushNotification = require("react-native-push-notification");

const INITIAL_STATE = {
  notifications: [],
  tasks: [],
  token: "",
  unsubscribeMethod: undefined,
  badge: 0,
  newsBadge: 0,
  tasksBadge: 0,
};

const notificationReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_FCM_TOKEN:
      return Object.assign({}, state, {
        token: action.token,
      });

    case SET_NOTIFICATIONS:
      return Object.assign({}, state, {
        notifications: action.notifications,
      });
    case ADD_NOTIFICATIONS:
      let allNotifications = state.notifications.concat(action.notifications);
      return Object.assign({}, state, {
        notifications: allNotifications,
      });
    case SET_TASKS:
      return Object.assign({}, state, {
        tasks: action.tasks,
      });
    case ADD_TASKS:
      let allTasks = state.tasks.concat(action.tasks);
      return Object.assign({}, state, {
        tasks: allTasks,
      });
    case SET_UNSUBSCRIBE_METHOD:
      return Object.assign({}, state, {
        unsubscribeMethod: action.method,
      });
    case SET_READED_NOTIFICATION:
      let badge = state.badge;
      let newsBadge = state.newsBadge;
      let notifications = [];
      state.notifications.forEach((n) => {
        if (n.id == action.id) {
          console.log(n);
          if (n.unread == true) {
            PushNotification.setApplicationIconBadgeNumber(state.badge - 1);
            badge -= 1;
            newsBadge -= 1;
          }
          n.unread = false;
        }
        notifications.push(n);
      });
      return Object.assign({}, state, {
        notifications: notifications,
        badge: badge,
        newsBadge: newsBadge,
      });

    case SET_READED_TASKS:
      badge = state.badge;
      let tasksBadge = state.tasksBadge;
      let tasks = [];
      state.tasks.forEach((n) => {
        if (n.id == action.id) {
          if (n.unread == true) {
            PushNotification.setApplicationIconBadgeNumber(state.badge - 1);
            badge -= 1;
            tasksBadge -= 1;
          }
          n.unread = false;
        }
        tasks.push(n);
      });
      return Object.assign({}, state, {
        tasks: tasks,
        badge: badge,
        tasksBadge: tasksBadge,
      });
    case ADD_NOTIFICATION:
      let newArray = state.notifications;
      newArray.unshift(action.notification);
      return Object.assign({}, state, {
        notifications: newArray,
      });
    case ADD_TASK:
      newArray = state.tasks;
      newArray.unshift(action.task);
      return Object.assign({}, state, {
        tasks: newArray,
      });
    case SET_BADGE_COUNT:
      return Object.assign({}, state, {
        badge: action.badge,
      });

    case SET_TASKS_BADGE:
      return Object.assign({}, state, {
        tasksBadge: action.badge,
      });
    case SET_NEWS_BADGE:
      return Object.assign({}, state, {
        newsBadge: action.badge,
      });
    case "CLEAR_STORE":
      PushNotification.setApplicationIconBadgeNumber(0);
      messaging().unregisterDeviceForRemoteMessages();
      if (state.unsubscribeMethod) {
        state.unsubscribeMethod();
      }
      return {
        notifications: [],
        unreadNotifications: [],
        homeScreenUnreadNotifications: [],
        unreadCount: 0,
        initialNotification: undefined,
        unsubscribeMethod: undefined,
        token: "",
        pages: 1,
      };
    default:
      return state;
  }
};

export default notificationReducer;

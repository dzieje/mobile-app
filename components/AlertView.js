import React, { Component } from "react";
import { StyleSheet, Animated, Easing, View } from "react-native";
import * as AppConstants from "../constants";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Label from "./Label";
import { showAlert } from "../store/alertReducer/actions";
import TouchableOpacity from "./TouchableOpacity";

class Alert extends Component {
  constructor(props) {
    super(props);
    this.state = {
      offestValue: new Animated.Value(20),
      show: false,
    };
    this.heightValue = 100;
    this.timeout = "";
    this.show = false;
  }
  componentDidMount() {
    this.heightValue = 75;
  }
  componentDidUpdate() {
    // if (navigationService.getRouteIndex() == 1) {
    //   this.heightValue =
    //     AppConstants.componentHeight +
    //     AppConstants.marginBottom +
    //     AppConstants.getSafeAreaBottom() +
    //     AppConstants.tabBarHeight;
    // } else {
    //   this.heightValue =
    //     AppConstants.componentHeight +
    //     AppConstants.marginBottom +
    //     AppConstants.getSafeAreaBottom();
    // }
    if (this.props.alertReducer.show == true && this.state.show == false) {
      this.show = true;
      this.setState({ show: true });
      Animated.timing(this.state.offestValue, {
        toValue: 20,
        duration: AppConstants.animationTimeMs,
        useNativeDriver: false,
      }).start(() => {
        const timeout = setTimeout(() => {
          this.closeAlert();
        }, 3000);
        this.timeout = timeout;
      });
    }
  }

  closeAlert() {
    clearTimeout(this.timeout);
    if (this.show == true) {
      this.props.showAlert(false);
      Animated.timing(this.state.offestValue, {
        toValue: -200,
        easing: Easing.fadeIn,
        duration: AppConstants.animationTimeMs,
        useNativeDriver: false,
      }).start(() => {
        this.setState({ show: false });
      });
    }
  }

  render() {
    if (this.state.show == false) {
      return <View></View>;
    } else {
      return (
        <Animated.View
          style={[
            styles.background,
            {
              bottom: this.state.offestValue,
              height: this.heightValue,
              backgroundColor: this.props.alertReducer.backgroundColor,
            },
          ]}
        >
          <TouchableOpacity
            style={styles.opacity}
            onPress={() => {
              this.closeAlert();
            }}
          >
            <Label style={styles.messageLabel} fontSize={14}>
              {this.props.alertReducer.message}
            </Label>
          </TouchableOpacity>
        </Animated.View>
      );
    }
  }
}

const styles = StyleSheet.create({
  opacity: {
    flex: 1,
    width: "90%",
    justifyContent: "center",
    alignItems: "center",
  },
  background: {
    position: "absolute",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: AppConstants.deviceWidth - 10,
    marginRight: 5,
    marginLeft: 5,
    borderRadius: AppConstants.borderRadius,
  },
  messageLabel: {
    color: "#fff",
    textAlign: "center",
    maxHeight: "100%",
  },
});

const mapStateToProps = (state) => {
  const { alertReducer } = state;
  return { alertReducer };
};
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      showAlert,
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(Alert);

import React, {Component} from 'react';
import {StyleSheet, Text, View, Platform} from 'react-native';
import * as AppConstants from '../constants';
import TouchableOpacity from './TouchableOpacity';
import LabelBold from './LabelBold';
import Styles from '../styles/styles';

export function ButtonOutline(props) {
  const styles = StyleSheet.create({
    buttonView: {
      display: 'flex',
      height: AppConstants.buttonHeight,
      width: AppConstants.buttonWidth,
      borderRadius: AppConstants.buttonCornerRadius,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'transparent',
      color: props.color ? props.color : '#fff',
      borderColor: props.color ? props.color : '#fff',
      borderWidth: 2,
    },
    buttonTouchableOpacity: {
      height: AppConstants.buttonHeight,
      width: AppConstants.buttonWidth,
      display: 'flex',
      justifyContent: 'center',
    },
    disabledBackgroundColor: {
      backgroundColor: AppConstants.greyDarkColor,
    },
    standardBackgroundColor: {
      backgroundColor: props.negative
        ? AppConstants.greyDarkColor
        : AppConstants.buttonColorActive,
    },
    text: {
      textAlign: 'center',
    },
  });
  return (
    <View style={[styles.buttonView, props.style, Styles.shadow]}>
      <TouchableOpacity
        style={styles.buttonTouchableOpacity}
        disabled={props.disabled ? props.disabled : false}
        onPress={() => props.onPress()}
        underlayColor={props.color ? props.color : '#fff'}
        activeOpacity={0.5}>
        <LabelBold
          style={[styles.text, {color: props.color ? props.color : '#fff'}]}>
          {props.title}
        </LabelBold>
      </TouchableOpacity>
    </View>
  );
}

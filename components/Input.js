import React, {Component} from 'react';
import {StyleSheet, TextInput, View, Text, Platform} from 'react-native';
import Label from './Label';
import * as AppConstants from '../constants';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import TouchableOpacity from './TouchableOpacity';
import Styles from '../styles/styles';

const {forwardRef, useRef, useImperativeHandle} = React;

const Input = forwardRef((props, ref) => {
  const [value, onChangeText] = React.useState(props.value ? props.value : '');
  const [secure, onSecureChange] = React.useState(
    props.secureTextEntry ? props.secureTextEntry : false,
  );
  const [fontSize, setFontSize] = React.useState(AppConstants.contentFontSize);
  const [contentWidth, setContentWidth] = React.useState(0);
  const inputEl = useRef(null);

  useImperativeHandle(ref, () => ({
    setFocus() {
      inputEl.current.focus();
    },
  }));

  function onUpdate(text) {
    onChangeText(text);
    if (props.onUpdate) {
      props.onUpdate(text);
    }
    if (Platform.OS === 'android') {
      checkFontSize(0);
    }
  }
  const validationMessage = props.validationMessage
    ? props.validationMessage
    : '';

  const styles = StyleSheet.create({
    inputComponent: {
      width: AppConstants.childsWidth,
      justifyContent: 'center',
      position: 'relative',
      marginBottom: props.bottom ? props.bottom : 0,
    },
    inputStyles: {
      backgroundColor: props.disabled
        ? AppConstants.inputColorDisabled
        : AppConstants.inputBackgroundColor,
      height: AppConstants.inputHeight,
      width: '100%',
      color: '#353535',
      borderRadius: AppConstants.inputHeight / 2,
      paddingLeft: AppConstants.inputHeight / 2,
      paddingRight: AppConstants.inputHeight / 2,
      borderColor:
        validationMessage.length > 0
          ? AppConstants.redColor
          : AppConstants.inputBackgroundColor,
      borderWidth: AppConstants.borderWidth,
    },
    previewImage: {
      position: 'absolute',
      right: 10,
      height: AppConstants.inputHeight,
      display: 'flex',
      justifyContent: 'center',
      width: 30,
      alignItems: 'center',
    },

    errorMessageLabel: {
      width: '100%',
      textAlign: 'left',
      minHeight: validationMessage.length > 0 ? 20 : 0,
      color: AppConstants.redColor,
      fontSize: 14,
      fontFamily: 'Nunito-Regular',
    },
  });

  function checkFontSize(width) {
    if (contentWidth == 0) return;
    if (Platform.OS === 'ios') {
      if (width > contentWidth * 0.8) {
        if (fontSize > 9) setFontSize(fontSize - 1);
      } else if (
        width < contentWidth * 0.5 &&
        fontSize < AppConstants.contentFontSize
      ) {
        setFontSize(fontSize + 1);
      }
    } else {
      if (value.length * fontSize * 0.6 > contentWidth * 0.8) {
        if (fontSize > 9) {
          setFontSize(fontSize - 1);
        }
      } else if (
        value.length * fontSize * 0.6 < contentWidth * 0.5 &&
        fontSize < AppConstants.contentFontSize
      ) {
        setFontSize(fontSize + 1);
      }
    }
  }

  function getPreviewIcon() {
    const canPreview = props.canPreview ? props.canPreview : false;
    if (canPreview) {
      return (
        <View style={styles.previewImage}>
          <TouchableOpacity
            onPress={() => {
              onSecureChange(!secure);
            }}>
            {/* <BlueIcon name="oko"></BlueIcon> */}
          </TouchableOpacity>
        </View>
      );
    } else {
      return null;
    }
  }

  function getKeyboardType(type) {
    if (type == null) {
      return 'default';
    }
    if (type === 'decimal-pad') {
      return Platform.OS === 'android'
        ? 'number-pad'
        : 'numbers-and-punctuation';
    }
    return type;
  }

  return (
    <View style={[styles.inputComponent, Styles.shadow]}>
      <Label>{props.placeholder ? props.placeholder : ''}</Label>
      <View>
        <TextInput
          clearTextOnFocus={false}
          selectionColor={AppConstants.blueColor}
          maxLength={props.maxLength ? props.maxLength : 255}
          ref={inputEl}
          returnKeyType={props.returnKey ? props.returnKey : 'default'}
          focus={true}
          onSubmitEditing={() => {
            props.onReturnPress ? props.onReturnPress() : '';
          }}
          blurOnSubmit={false}
          onLayout={(event) => {
            setContentWidth(event.nativeEvent.layout.width);
          }}
          onContentSizeChange={(size) => {
            checkFontSize(size.nativeEvent.contentSize.width);
          }}
          editable={props.disabled ? !props.disabled : true}
          selectTextOnFocus={props.disabled ? !props.disabled : true}
          placeholder={''}
          secureTextEntry={secure}
          style={[
            styles.inputStyles,
            {
              fontFamily: 'Nunito-Regular',
              fontSize: fontSize,
            },
          ]}
          onChangeText={(text) => {
            onUpdate(text);
          }}
          keyboardType={getKeyboardType(props.keyboardType)}
          value={value}
        />
        {getPreviewIcon()}
      </View>
      {validationMessage.length > 0 ? (
        <Text numberOfLines={0} style={styles.errorMessageLabel}>
          {validationMessage}
        </Text>
      ) : (
        <View></View>
      )}
    </View>
  );
});

const mapStateToProps = (state) => {
  const {appReducer} = state;
  return {appReducer};
};
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({}, dispatch);
};
export default connect(mapStateToProps, mapDispatchToProps, null, {
  forwardRef: true,
})(Input);

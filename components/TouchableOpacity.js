import React, { Component } from "react";
import { Platform } from "react-native";
import { TouchableOpacity as IOSButton } from "react-native-gesture-handler";
import { TouchableOpacity as AndroidButton } from "react-native";

export default class TouchableOpacity extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    if (Platform.OS === "ios") {
      return (
        <IOSButton
          hitSlop={{ right: 10, left: 10, top: 10, bottom: 10 }}
          {...this.props}
        >
          {this.props.children}
        </IOSButton>
      );
    } else {
      return (
        <AndroidButton
          hitSlop={{ right: 10, left: 10, top: 10, bottom: 10 }}
          {...this.props}
        >
          {this.props.children}
        </AndroidButton>
      );
    }
  }
}

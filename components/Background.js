import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
} from "react-native";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import { IOSKeyboardAvoidingView } from "./IOSKeyboardAvoidingView";
import LinearGradient from "react-native-linear-gradient";
import * as AppConstans from "../constants";

export class Background extends Component {
  constructor(props) {
    super(props);
    this.yOffset = 0;
  }

  scrollTo(position) {
    if (this.scrollView) {
      this.scrollView.scrollTo({ y: position + this.yOffset, animated: true });
    }
  }

  scrollToTop() {
    if (this.scrollView) {
      this.scrollView.scrollTo({ y: 0, animated: true });
    }
  }

  getView(withScroll) {
    if (withScroll) {
      return (
        <ScrollView
          scrollEventThrottle={20}
          contentContainerStyle={{ flexGrow: 1, height: "100%" }}
          automaticallyAdjustContentInsets={false}
          onScroll={(event) => {
            this.yOffset = event.nativeEvent.contentOffset.y;
          }}
          ref={(view) => {
            this.scrollView = view;
          }}
          alwaysBounceHorizontal="false"
          alwaysBounceVertical="false"
          bounces={this.props.bounces ? this.props.bounces : true}
        >
          <TouchableWithoutFeedback
            accessible={false}
            style={[styles.background, this.props.style]}
            onPress={() => Keyboard.dismiss()}
          >
            {this.props.children}
          </TouchableWithoutFeedback>
        </ScrollView>
      );
    } else {
      return (
        <TouchableWithoutFeedback
          accessible={false}
          style={[styles.background, this.props.style]}
          onPress={() => Keyboard.dismiss()}
        >
          {this.props.children}
        </TouchableWithoutFeedback>
      );
    }
  }
  render() {
    const scrollView = this.props.scrollView ? true : false;
    return (
      <LinearGradient
        locations={[0.1, 1]}
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 0 }}
        colors={["#36B9CC", "#258391"]}
        style={[styles.gradient, this.props.style]}
      >
        {Platform.OS === "ios" ? (
          <IOSKeyboardAvoidingView>
            {this.getView(scrollView)}
          </IOSKeyboardAvoidingView>
        ) : (
          this.getView(scrollView)
        )}
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  background: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    minHeight: AppConstans.minContentHeight,
    paddingTop: AppConstans.getStatusBarHeight(),
  },
  gradient: {
    flex: 1,
  },
});

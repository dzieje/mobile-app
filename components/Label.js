import React, { Component } from "react";
import { StyleSheet, Text } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

export default class Label extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Text
        letterSpacing={0}
        includeFontPadding={false}
        onLayout={(element) => {
          this.props.onLayout ? this.props.onLayout(element) : 1;
        }}
        numberOfLines={0}
        {...this.props}
        style={[
          styles.labelComponent,
          {
            fontFamily: "Nunito-Regular",
          },
          this.props.style,
        ]}
      >
        {this.props.children ? this.props.children : ""}
      </Text>
    );
  }
}

const styles = StyleSheet.create({
  labelComponent: {
    marginBottom: 5,
    color: "#fff",
    padding: 0,
    fontSize: 15,
  },
});

const mapStateToProps = (state) => {
  const { appReducer } = state;
  return { appReducer };
};
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({}, dispatch);
};
// export default connect(mapStateToProps, mapDispatchToProps)(Label);

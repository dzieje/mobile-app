import React, {Component} from 'react';
import {
  KeyboardAvoidingView,
  NativeModules,
  NativeEventEmitter,
} from 'react-native';

const {StatusBarManager} = NativeModules;

export class IOSKeyboardAvoidingView extends Component {
  state = {statusBarHeight: 0};

  componentDidMount() {
    StatusBarManager.getHeight((statusBarFrameData) => {
      this.setState({statusBarHeight: statusBarFrameData.height});
    });
    const statusBarEmiterManager = new NativeEventEmitter(StatusBarManager);
    this.statusBarListener = statusBarEmiterManager.addListener(
      'statusBarFrameWillChange',
      (statusBarData) => {
        this.setState({statusBarHeight: statusBarData.frame.height});
      },
    );
  }

  componentWillUnmount() {
    this.statusBarListener.remove();
  }

  render() {
    const {style, children} = this.props;
    return (
      <KeyboardAvoidingView behavior="padding" style={style}>
        {children}
      </KeyboardAvoidingView>
    );
  }
}

import React, { Component } from "react";
import { StyleSheet, ActivityIndicator, Animated } from "react-native";
import * as AppConstants from "../constants";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { setLoaderRef } from "../store/appReducer/actions";

class LoaderView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      opacityValue: new Animated.Value(0),
      isShowed: false,
    };
  }
  componentDidMount() {
    this.props.setLoaderRef(this);
  }
  componentDidUpdate() {}

  showLoader() {
    this.setState({ isShowed: true });
    Animated.timing(this.state.opacityValue, {
      toValue: 0.8,
      duration: 1,
      useNativeDriver: true,
    }).start(() => {});
  }

  hideLoader() {
    Animated.timing(this.state.opacityValue, {
      toValue: 0,
      duration: 1,
      useNativeDriver: true,
    }).start(() => {
      this.setState({ isShowed: false });
    });
  }

  render() {
    if (this.state.isShowed == true) {
      return (
        <Animated.View
          style={[styles.background, { opacity: this.state.opacityValue }]}
        >
          <ActivityIndicator
            size={"large"}
            color={AppConstants.yellowColor}
          ></ActivityIndicator>
        </Animated.View>
      );
    } else {
      return null;
    }
  }
}

const styles = StyleSheet.create({
  background: {
    backgroundColor: "rgba(255,255,255,0.5)",
    position: "absolute",
    width: AppConstants.deviceWidth,
    height: AppConstants.deviceHeight,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
});

const mapStateToProps = (state) => {
  const { appReducer } = state;
  return { appReducer };
};
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      setLoaderRef,
    },
    dispatch
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(LoaderView);

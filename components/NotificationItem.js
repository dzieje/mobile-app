import React, { Component } from "react";
import { StyleSheet, View, Keyboard, Text, Image } from "react-native";
import * as AppConstants from "../constants";
import Styles from "../styles/styles";
import Label from "./Label";
import LabelBold from "./LabelBold";
import Spacing from "./Spacing";
import TouchableOpacity from "./TouchableOpacity";

export default class NotificationItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imageLoaded: false,
    };
  }
  componentDidMount() {
    // LayoutAnimation.configureNext(LayoutAnimation.Presets.linear);
  }
  componentDidUpdate() {}

  parseDate(dateString) {
    let months = [
      "sty",
      "lut",
      "mar",
      "kwi",
      "maj",
      "cze",
      "lip",
      "sie",
      "wrz",
      "paź",
      "lis",
      "gru",
    ];
    if (dateString && dateString != "") {
      let date = new Date(dateString);
      var dd = String(date.getDate()).padStart(2, "0");
      var mm = String(date.getMonth() + 1).padStart(2, "0"); //January is 0!
      var yyyy = date.getFullYear();

      var hh = String(date.getHours()).padStart(2, "0");
      var min = String(date.getMinutes()).padStart(2, "0");

      return (
        hh +
        ":" +
        min +
        " | " +
        dd +
        " " +
        months[date.getMonth()] +
        " " +
        String(date.getFullYear())
      );
    }
    return "";
  }

  getPhoto() {
    if (this.props.item.photo == null) {
      if (this.state.imageLoaded == false) {
        // this.setState({imageLoaded: true})
      }
    } else {
      return (
        <Image
          onError={() => this.onImageLoadCompleted()}
          onLoadEnd={() => this.onImageLoadCompleted()}
          style={[styles.image]}
          source={{
            uri: this.props.item.photo,
            method: "GET",
            headers: {},
          }}
        ></Image>
      );
    }
    return <View></View>;
  }
  onImageLoadCompleted() {
    this.setState({ imageLoaded: true });
  }

  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <TouchableOpacity
          onPress={() => {
            this.props.onPress ? this.props.onPress() : "";
          }}
        >
          <Spacing></Spacing>
          <Spacing></Spacing>
          <View style={styles.box}>
            <View style={styles.imageBox}>{this.getPhoto()}</View>
            <View style={styles.notificationBox}>
              <LabelBold
                style={{ color: "#000", fontSize: 11, marginBottom: 0 }}
              >
                {this.props.item ? this.props.item.title : ""}
              </LabelBold>
              <Label style={{ color: "#000", fontSize: 10, marginBottom: 0 }}>
                {this.props.item ? this.props.item.content : ""}
              </Label>
              <Label
                style={{
                  color: AppConstants.greyColor,
                  fontSize: 9,
                  width: "100%",
                  marginBottom: 0,
                }}
              >
                {this.props.item ? this.parseDate(this.props.item.date) : ""}
              </Label>

              <Label
                style={{
                  color: AppConstants.greyColor,
                  fontSize: 9,
                  width: "100%",
                  marginBottom: 0,
                }}
              >
                {this.props.item ? this.props.item.author : ""}
              </Label>
            </View>
          </View>
          {this.props.item.unread ? (
            <View style={styles.unreadBox}>
              <Label style={styles.unreadLabel}>Nowy</Label>
            </View>
          ) : (
            <View></View>
          )}
          <Spacing></Spacing>
          <Spacing></Spacing>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginLeft: 5,
    marginRight: 5,
    borderBottomColor: AppConstants.blueColor,
    borderBottomWidth: 2,
    paddingTop: 5,
  },
  imageBox: {
    height: AppConstants.componentHeight,
    width: AppConstants.componentHeight,
    borderRadius: AppConstants.componentHeight / 2,
    backgroundColor: AppConstants.blueColor,
  },
  box: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
    width: "100%",
  },
  notificationBox: {
    flex: 1,
    height: "100%",
    flexDirection: "column",
    marginLeft: 5,
  },
  dot: {
    height: 10,
    width: 10,
    borderRadius: 5,
    backgroundColor: AppConstants.redColor,
    marginRight: 1,
  },
  footer: {
    justifyContent: "flex-start",
    alignItems: "flex-start",
    width: "100%",
  },
  image: {
    overflow: "hidden",
    height: "100%",
    width: "100%",
    borderRadius: AppConstants.componentHeight / 2,
  },
  unreadBox: {
    backgroundColor: AppConstants.yellowColor,
    position: "absolute",
    top: 0,
    right: 0,
    borderRadius: 5,
    paddingRight: 5,
    paddingLeft: 5,
  },
  unreadLabel: {
    color: "#fff",
    marginBottom: 0,
    fontSize: 9,
  },
});

import React, {Component} from 'react';
import {StyleSheet, Text} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Label from './Label';

export default class LabelBold extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Label
        onLayout={(element) => {
          this.props.onLayout ? this.props.onLayout(element) : 1;
        }}
        numberOfLines={0}
        {...this.props}
        style={[
          {
            fontFamily: 'Nunito-Bold',
          },
          this.props.style,
        ]}>
        {this.props.children}
      </Label>
    );
  }
}

const styles = StyleSheet.create({
  labelComponent: {
    marginBottom: 5,
    color: '#fff',
    padding: 0,
    fontSize: 15,
  },
});

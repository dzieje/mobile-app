import React, { Component } from "react";
import { StyleSheet, View, Keyboard, Text, Image } from "react-native";
import * as AppConstants from "../constants";
import Styles from "../styles/styles";
import { ministryLogo } from "../assets/imagesList";

export default class CView extends Component {
  render() {
    return (
      <View style={[styles.content, Styles.shadow, this.props.style]}>
        <View style={[styles.childrenContainer, this.props.childrenStyle]}>
          {this.props.children}
        </View>
        <Image style={styles.footerImage} source={ministryLogo}></Image>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    backgroundColor: "#fff",

    borderRadius: AppConstants.borderRadius,
    overflow: "hidden",
  },
  childrenContainer: {
    flex: 1,
    width: AppConstants.deviceWidth * 0.9,
  },
  footerImage: {
    resizeMode: "contain",
    height: 80,
    width: "100%",
  },
});

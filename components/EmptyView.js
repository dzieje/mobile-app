import React, { Component } from "react";
import { StyleSheet, Animated, Easing, View } from "react-native";
import * as AppConstants from "../constants";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Label from "./Label";
import { showAlert } from "../store/alertReducer/actions";
import TouchableOpacity from "./TouchableOpacity";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { faNewspaper } from "@fortawesome/free-solid-svg-icons";
import LabelBold from "./LabelBold";

export default class EmptyView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      offestValue: new Animated.Value(20),
      show: false,
    };
    this.heightValue = 100;
    this.timeout = "";
    this.show = false;
  }
  componentDidMount() {
    this.heightValue = 75;
  }
  componentDidUpdate() {
    // if (navigationService.getRouteIndex() == 1) {
    //   this.heightValue =
    //     AppConstants.componentHeight +
    //     AppConstants.marginBottom +
    //     AppConstants.getSafeAreaBottom() +
    //     AppConstants.tabBarHeight;
    // } else {
    //   this.heightValue =
    //     AppConstants.componentHeight +
    //     AppConstants.marginBottom +
    //     AppConstants.getSafeAreaBottom();
    // }
  }
  render() {
    return (
      <View style={styles.background}>
        <FontAwesomeIcon
          size={50}
          color={AppConstants.blueColor}
          icon={faNewspaper}
        ></FontAwesomeIcon>
        <LabelBold style={{ color: AppConstants.blueColor, fontSize: 35 }}>
          {this.props.title ? this.props.title : "Brak danych"}
        </LabelBold>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  background: {
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center",
    flexDirection: "column",
    flex: 1,
  },
  messageLabel: {
    color: "#fff",
    textAlign: "center",
    maxHeight: "100%",
  },
});

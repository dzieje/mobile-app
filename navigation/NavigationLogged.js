import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import HomeScreen from "../screens/Home/HomeScreen";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import {
  faCoffee,
  faUser,
  faEnvelopeOpenText,
  faCalendarAlt,
  faPencilAlt,
  faTasks,
} from "@fortawesome/free-solid-svg-icons";
import * as AppConstants from "../constants";
import LabelBold from "../components/LabelBold";
import { View } from "react-native";
import CalendarScreen from "../screens/CalendarScreen/CalendarScreen";
import TasksScreen from "../screens/TasksScreen/TasksScreen";
import ProfileScreen from "../screens/ProfileScreen/ProfileScreen";
import { createStackNavigator } from "@react-navigation/stack";
import NewsDetailsScreen from "../screens/Home/NewsDetailsScreen";

function HomeStackScreen() {
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator
      initialRouteName="News"
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="News" component={HomeScreen} />
      <Stack.Screen name="NewsDetails" component={NewsDetailsScreen} />
    </Stack.Navigator>
  );
}

function TasksStackScreen() {
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator
      initialRouteName="Tasks"
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="Tasks" component={TasksScreen} />
      <Stack.Screen name="TasksDetails" component={NewsDetailsScreen} />
    </Stack.Navigator>
  );
}

function NavigationLogged(props) {
  const Tab = createBottomTabNavigator();

  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;
          if (route.name === "Home") {
            iconName = faEnvelopeOpenText;
          } else if (route.name == "Calendar") {
            iconName = faCalendarAlt;
          } else if (route.name == "Profile") {
            iconName = faUser;
          } else if (route.name == "Tasks") {
            iconName = faTasks;
          }
          return <FontAwesomeIcon icon={iconName} color={color} size={25} />;
        },
        tabBarLabel: ({ focused, color }) => {
          let name;
          if (route.name == "Home") {
            name = "Aktualności";
          } else if (route.name == "Calendar") {
            name = "Kalendarz";
          } else if (route.name == "Profile") {
            name = "Profil";
          } else if (route.name == "Tasks") {
            name = "Zadania";
          }
          return (
            <View
              style={{
                alignItems: "center",
                flex: 1,
                justifyContent: "center",
              }}
            >
              <LabelBold
                style={{
                  color: color,
                  fontSize: 12,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                {name}
              </LabelBold>
            </View>
          );
        },
      })}
      lazy={false}
      tabBarOptions={{
        activeTintColor: AppConstants.darkBlueColor,
        inactiveTintColor: AppConstants.greyColor,
        safeAreaInsets: { bottom: AppConstants.getSafeAreaBottom() },
        timingConfig: 1,
        position: "absolute",
        keyboardHidesTabBar: false,
        showLabel: true,
        showIcon: true,
        labelStyle: {
          flex: 1,
          maxWidth: AppConstants.deviceWidth / 4,
        },
        tabStyle: {
          paddingTop: 5,
          borderTopWidth: 1,
          borderTopColor: AppConstants.darkBlueColor,
          backgroundColor: "#fff",
        },
        style: {
          height: AppConstants.getSafeAreaBottom() + AppConstants.tabBarHeight,
          backgroundColor: "#fff",
        },
      }}
    >
      <Tab.Screen
        options={{
          tabBarBadge:
            props.notificationReducer.newsBadge == 0
              ? null
              : props.notificationReducer.newsBadge,
        }}
        name="Home"
        component={HomeStackScreen}
      />
      <Tab.Screen
        name="Tasks"
        component={TasksStackScreen}
        options={{
          tabBarBadge:
            props.notificationReducer.tasksBadge == 0
              ? null
              : props.notificationReducer.tasksBadge,
        }}
      />
      {/* <Tab.Screen name="Calendar" component={CalendarScreen} /> */}
      <Tab.Screen name="Profile" component={ProfileScreen} />
    </Tab.Navigator>
  );
}

const mapStateToProps = (state) => {
  const { appReducer, notificationReducer } = state;
  return { appReducer, notificationReducer };
};
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({}, dispatch);
};

export default connect(mapStateToProps, {})(NavigationLogged);

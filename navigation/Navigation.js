const { NavigationContainer } = require("@react-navigation/native");
const { default: LoginScreen } = require("../screens/LoginScreen/LoginScreen");
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import React, { Component } from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { View } from "react-native";
import NavigationLogged from "./NavigationLogged";
import SplashScreen from "react-native-splash-screen";
import * as AppConstants from "../constants";
import * as SecureStore from "expo-secure-store";
import { setRefreshToken, refreshToken } from "../store/sessionReducer/actions";
import { setLoggedIn } from "../store/appReducer/actions";
import { navigationRef } from "./navigationService";
import messaging from "@react-native-firebase/messaging";

const Stack = createStackNavigator();
const notLoggedView = (
  <Stack.Navigator initialState="Login">
    <Stack.Screen
      name="Login"
      component={LoginScreen}
      options={{ headerShown: false }}
    />
  </Stack.Navigator>
);

// const Navigation = function Navigation(props) {
//   return (
//     <NavigationContainer>
//       {props.appReducer.loggedIn ? (
//         <NavigationLogged></NavigationLogged>
//       ) : (
//         notLoggedView
//       )}
//     </NavigationContainer>
//   );
// };

class Navigation extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.checkToken();
  }

  checkToken() {
    SecureStore.getItemAsync(AppConstants.refreshTokenName).then(
      (resp) => {
        if (resp) {
          console.log(resp);
          this.props.setRefreshToken(resp);
          this.props.refreshToken(resp).then(
            (res) => {
              this.props.setLoggedIn(true);
              SplashScreen.hide();
            },
            (err) => {
              SplashScreen.hide();
            }
          );
        } else {
          SplashScreen.hide();
        }
      },
      (error) => {
        SplashScreen.hide();
      }
    );
  }
  render() {
    return (
      <NavigationContainer ref={navigationRef}>
        {this.props.appReducer.loggedIn ? (
          <NavigationLogged></NavigationLogged>
        ) : (
          notLoggedView
        )}
      </NavigationContainer>
    );
  }
}

const mapStateToProps = (state) => {
  const { appReducer } = state;
  return { appReducer };
};
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    { setRefreshToken, refreshToken, setLoggedIn },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(Navigation);

import React, { Component } from "react";
import { StyleSheet, View, Keyboard, Text, Image } from "react-native";
import { Background } from "../../components/Background";
import LabelBold from "../../components/LabelBold";
import Spacing from "../../components/Spacing";
import Style from "../../styles/styles";
import * as AppConstants from "../../constants";
import CView from "../../components/CView";
import { FlatList } from "react-native-gesture-handler";
import NotificationItem from "../../components/NotificationItem";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import TouchableOpacity from "../../components/TouchableOpacity";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { faCameraRetro } from "@fortawesome/free-solid-svg-icons";
import { ButtonOutline } from "../../components/ButtonOutline";
import { getUserInfo } from "../../store/appReducer/actions";
import { logout } from "../../store/sessionReducer/actions";
import Label from "../../components/Label";
import { defaultProfileImage } from "../../assets/imagesList";
import { Button } from "../../components/Button";

class ProfileScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: new Date(),
    };
  }
  componentDidMount() {
    this.props.getUserInfo().then((resp) => {
      this.setState({ date: new Date() });
    });
  }

  parseDate(dateString) {
    if (dateString && dateString != "") {
      let date = new Date(dateString);
      var dd = String(date.getDate()).padStart(2, "0");
      var mm = String(date.getMonth() + 1).padStart(2, "0"); //January is 0!
      var yyyy = date.getFullYear();

      return dd + "-" + mm + "-" + yyyy;
    }
    return "";
  }

  onButtonPress() {
    this.props.logout().then(() => {});
  }
  getImage() {
    if (this.props.appReducer.profileData.avatar_filename.length > 0) {
      console.log(
        AppConstants.URL +
          "api/avatars/" +
          this.props.appReducer.profileData.avatar_filename
      );
      return (
        <Image
          onLayout={(event) => {
            // var { x, y, width, height } = event.nativeEvent.layout;
            // this.setState({ imageBorder: width / 2 });
          }}
          style={[{ resizeMode: "cover", width: "100%", height: "100%" }]}
          source={{
            uri:
              AppConstants.URL +
              "avatars/" +
              this.props.appReducer.profileData.avatar_filename +
              "?" +
              this.state.date,
            method: "GET",
            headers: {
              Pragma: "no-cache",
              Authorization: "Bearer " + this.props.sessionReducer.accessToken,
            },
            CACHE: "reload",
          }}
        ></Image>
      );
    } else {
      return (
        <Image
          style={{ resizeMode: "cover", width: "100%", height: "100%" }}
          source={defaultProfileImage}
        ></Image>
      );
    }
  }
  render() {
    return (
      <Background scrollView={false}>
        <Spacing></Spacing>
        <LabelBold style={Style.title}>Mój Profil</LabelBold>
        <CView
          style={[Style.container, styles.flexContainer]}
          childrenStyle={{ alignItems: "center", justifyContent: "center" }}
        >
          <Spacing></Spacing>
          <Spacing></Spacing>
          <View style={[styles.circularProfile]}>
            {this.getImage()}
            {/* <TouchableOpacity>
              <View style={styles.editProfileImageView}>
                <FontAwesomeIcon
                  icon={faCameraRetro}
                  color={AppConstants.yellowColor}></FontAwesomeIcon>
              </View>
            </TouchableOpacity> */}
          </View>
          <View style={styles.content}>
            <Label style={[styles.textField, Style.title]}>
              {this.props.appReducer.profileData.name}{" "}
              {this.props.appReducer.profileData.surname}
            </Label>
            <LabelBold style={[styles.textField]}>
              {this.props.appReducer.profileData.email}
            </LabelBold>
            <Label style={[styles.textField]}>
              {this.parseDate(this.props.appReducer.profileData.date_of_birth)}
            </Label>
            <View style={styles.buttonArea}>
              <Button
                onPress={() => {
                  this.onButtonPress();
                }}
                color={AppConstants.blueColor}
                title={"Wyloguj"}
              ></Button>
            </View>
          </View>
        </CView>
        <Spacing></Spacing>
        <Spacing></Spacing>
        <Spacing></Spacing>
        <Spacing></Spacing>
      </Background>
    );
  }
}
const styles = StyleSheet.create({
  flexContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  textField: {
    marginBottom: 0,
    color: AppConstants.greyColor,
  },
  circularProfile: {
    height: AppConstants.deviceWidth * 0.4,
    width: AppConstants.deviceWidth * 0.4,
    borderRadius: (AppConstants.deviceWidth * 0.4) / 2,
    backgroundColor: AppConstants.blueColor,
    overflow: "hidden",
    justifyContent: "flex-end",
  },
  editProfileImageView: {
    width: "100%",
    height: AppConstants.deviceWidth * 0.1,
    backgroundColor: "#fff",
    opacity: 0.7,
    justifyContent: "center",
    alignItems: "center",
  },
  content: {
    flex: 1,

    alignItems: "center",
  },
  buttonArea: {
    flex: 1,
    justifyContent: "flex-end",
  },
});

const mapStateToProps = (state) => {
  const { appReducer, sessionReducer } = state;
  return { appReducer, sessionReducer };
};
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ getUserInfo, logout }, dispatch);
};
export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen);

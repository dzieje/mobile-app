import React, { Component } from "react";
import { StyleSheet, View, Platform, PermissionsAndroid } from "react-native";
import { Background } from "../../components/Background";
import LabelBold from "../../components/LabelBold";
import Spacing from "../../components/Spacing";
import Style from "../../styles/styles";
import * as AppConstants from "../../constants";
import CView from "../../components/CView";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  registerAppWithFCM,
  registerByToken,
  getNotifications,
} from "../../store/notificationReducer/actions";
import { ButtonOutline } from "../../components/ButtonOutline";
import Label from "../../components/Label";
import WebView from "react-native-webview";
import { FileSystem } from "react-native-unimodules";
import RNFetchBlob from "rn-fetch-blob";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { faPaperclip, faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import TouchableOpacity from "../../components/TouchableOpacity";
import * as Sharing from "expo-sharing";
import { showLoader } from "../../store/appReducer/actions";

class NewsDetailsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notification: { title: "", type: "" },
      type: "",
    };
  }
  componentDidMount() {
    this.notification = this.props.route.params.notification;
    let type = this.props.route.params.type;
    this.setState({ notification: this.notification, type: type ? type : "" });
  }

  parseDate(dateString) {
    let months = [
      "sty",
      "lut",
      "mar",
      "kwi",
      "maj",
      "cze",
      "lip",
      "sie",
      "wrz",
      "paź",
      "lis",
      "gru",
    ];
    if (dateString && dateString != "") {
      let date = new Date(dateString);
      var dd = String(date.getDate()).padStart(2, "0");
      var mm = String(date.getMonth() + 1).padStart(2, "0"); //January is 0!
      var yyyy = date.getFullYear();

      var hh = String(date.getHours()).padStart(2, "0");
      var min = String(date.getMinutes()).padStart(2, "0");

      return (
        hh +
        ":" +
        min +
        " | " +
        dd +
        " " +
        months[date.getMonth()] +
        " " +
        String(date.getFullYear())
      );
    }
    return "";
  }

  parseDateWithoutHours(dateString) {
    let months = [
      "sty",
      "lut",
      "mar",
      "kwi",
      "maj",
      "cze",
      "lip",
      "sie",
      "wrz",
      "paź",
      "lis",
      "gru",
    ];
    if (dateString && dateString != "") {
      let date = new Date(dateString);
      var dd = String(date.getDate()).padStart(2, "0");
      var mm = String(date.getMonth() + 1).padStart(2, "0"); //January is 0!
      var yyyy = date.getFullYear();

      var hh = String(date.getHours()).padStart(2, "0");
      var min = String(date.getMinutes()).padStart(2, "0");

      return (
        dd + " " + months[date.getMonth()] + " " + String(date.getFullYear())
      );
    }
    return "";
  }

  getHtmlView(content) {
    return (
      "<head><meta charset='UTF-8'><link href='https://fonts.googleapis.com/css2?family=Nunito&display=swap' rel='stylesheet'></head><meta name='viewport' content='initial-scale=1.0, maximum-scale=1.0'><body><div id='wrapper'>" +
      content +
      "</div></body><style>body{font-family: 'Nunito', sans-serif;text-align: left;font-size: 10pt;margin-top: 0; max-width:100%}</style>"
    );
  }

  getFileName(url) {
    let lastIndex = String(url).lastIndexOf("/");
    lastIndex = lastIndex != -1 ? lastIndex : 0;

    return String(url).substr(lastIndex + 1);
  }
  downloadAttachment(item) {
    if (Platform.OS === "ios") {
      this.props.showLoader(true);
      FileSystem.downloadAsync(
        item.path.replace(" ", "%20"),
        FileSystem.documentDirectory + item.filename.replace(" ", "_"),
        {
          headers: {
            Authorization: "Bearer " + this.props.sessionReducer.accessToken,
          },
        }
      )
        .then(({ uri }) => {
          this.props.showLoader(false);
          Sharing.shareAsync(uri, {});
        })
        .catch((error) => {
          this.props.showLoader(false);
          console.error(error);
        });
    } else {
      const { fs } = RNFetchBlob;
      // const downloads = fs.dirs.DocumentDir;
      var RNFS = require("react-native-fs");
      const granted = PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: "Dostęp do multimediów",
          message:
            "Czy chcesz zezwolić aplikacji na dostęp do multimediów i plików na urządzeniu?",
          buttonNeutral: "Zapytaj mnie później",
          buttonNegative: "Nie pozwalaj",
          buttonPositive: "Pozwól",
        }
      ).then((granted) => {
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          RNFetchBlob.config({
            addAndroidDownloads: {
              useDownloadManager: true, // <-- this is the only thing required
              notification: true,
              path:
                RNFS.DownloadDirectoryPath +
                "/" +
                item.filename.replace(" ", "_"),
            },
          })
            .fetch("GET", item.path.replace(" ", "%20"), {
              Authorization: "Bearer " + this.props.sessionReducer.accessToken,
            })
            .then(
              (res) => {},
              (error) => {}
            )
            .catch((err) => {});
        } else {
        }
      });
    }
  }
  getAttachmentsView() {
    if (this.state.notification.attachments) {
      return this.state.notification.attachments.map((item, index) => {
        return (
          <TouchableOpacity
            key={index}
            onPress={() => {
              this.downloadAttachment(item);
            }}
            style={styles.attachmentView}
          >
            <FontAwesomeIcon
              icon={faPaperclip}
              color={AppConstants.blueColor}
              size={15}
            />
            <Label
              style={[
                Style.text,
                styles.textField,
                {
                  paddingLeft: 5,
                  marginBottom: 0,
                  flex: 1,
                },
              ]}
            >
              {item.filename}
            </Label>
          </TouchableOpacity>
        );
      });
    } else {
      return <View></View>;
    }
  }
  getProjectNameView() {
    if (this.state.notification.project) {
      return (
        <View>
          <Spacing></Spacing>
          <Label style={styles.projectNameTextField}>
            {this.state.notification.project}
          </Label>
          <Spacing></Spacing>
        </View>
      );
    } else {
      return <View></View>;
    }
  }

  getValidTitleView() {
    if (this.state.notification.valid_from) {
      return (
        <View>
          <Spacing></Spacing>
          <Label style={styles.projectNameTextField}>
            {this.state.notification.structure}
          </Label>
          <Label style={styles.projectNameTextField}>
            {this.parseDateWithoutHours(this.state.notification.valid_from)} -{" "}
            {this.parseDateWithoutHours(this.state.notification.valid_to)}
          </Label>
          <Spacing></Spacing>
        </View>
      );
    } else {
      return <View></View>;
    }
  }
  render() {
    return (
      <Background scrollView={true}>
        <Spacing></Spacing>
        <View style={styles.headerContent}>
          <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
            <FontAwesomeIcon icon={faArrowLeft} color={"#fff"} size={15} />
          </TouchableOpacity>
          <LabelBold style={[Style.title, { flex: 1, textAlign: "center" }]}>
            Powiadomienie
          </LabelBold>
        </View>
        <CView style={[Style.container]}>
          <Spacing></Spacing>
          <View style={styles.flexContainer}>
            <View style={styles.dateBox}>
              <View>
                <Label style={[styles.textField, styles.dateTextField]}>
                  {this.parseDate(this.state.notification.date)}
                </Label>
                <Label style={[styles.textField, styles.dateTextField]}>
                  {this.state.notification.author}
                </Label>
              </View>
              {this.state.type == "task"
                ? this.getValidTitleView()
                : this.getProjectNameView()}
            </View>
            <LabelBold style={[styles.textField, styles.titleTextField]}>
              {this.state.notification.title}
            </LabelBold>
            <Spacing></Spacing>

            <View style={styles.box}>
              <View style={styles.border}></View>
              <WebView
                containerStyle={styles.webviewContainer}
                scalesPageToFit={true}
                source={{
                  html: this.getHtmlView(this.state.notification.content),
                }}
              ></WebView>
              {this.getAttachmentsView()}
              <View style={styles.border}></View>
            </View>
          </View>
        </CView>
        <Spacing></Spacing>
        <Spacing></Spacing>
        <Spacing></Spacing>
        <Spacing></Spacing>
      </Background>
    );
  }
}
const styles = StyleSheet.create({
  flexContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
  },
  textField: {
    fontSize: 10,
    marginBottom: 0,
    color: AppConstants.greyColor,
  },
  dateBox: {
    flexDirection: "row",
    width: "100%",
    justifyContent: "space-between",
    alignItems: "center",
    paddingRight: 10,
    paddingLeft: 10,
  },
  dateTextField: {
    fontSize: 10,
  },
  titleTextField: {
    fontSize: 15,
    color: AppConstants.blueColor,
    textAlign: "center",
  },
  projectNameTextField: {
    fontSize: 10,
    color: AppConstants.greyColor,
    textAlign: "center",
    marginBottom: 0,
  },
  webviewContainer: { flex: 1, width: "100%" },
  box: {
    flex: 1,
    width: "100%",
    alignItems: "center",
  },
  border: {
    height: 1,
    width: AppConstants.deviceWidth * 0.9 - 20,
    backgroundColor: AppConstants.darkBlueColor,
  },
  attachmentView: {
    width: "100%",
    padding: 5,
    flexDirection: "row",
  },
  headerContent: {
    flexDirection: "row",
    width: AppConstants.deviceWidth * 0.9,
    alignItems: "center",
    justifyContent: "center",
  },
});

const mapStateToProps = (state) => {
  const { appReducer, notificationReducer, sessionReducer } = state;
  return { appReducer, notificationReducer, sessionReducer };
};
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    { registerAppWithFCM, registerByToken, getNotifications, showLoader },
    dispatch
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(NewsDetailsScreen);

import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Keyboard,
  Text,
  Image,
  Button,
  RefreshControl,
  AppState,
} from "react-native";
import { Background } from "../../components/Background";
import LabelBold from "../../components/LabelBold";
import Spacing from "../../components/Spacing";
import Style from "../../styles/styles";
import * as AppConstants from "../../constants";
import CView from "../../components/CView";
import { FlatList, TouchableOpacity } from "react-native-gesture-handler";
import NotificationItem from "../../components/NotificationItem";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import messaging from "@react-native-firebase/messaging";
import {
  registerAppWithFCM,
  registerByToken,
  getNotifications,
  setReadedMessage,
  getNotificationDetails,
  setBadge,
  setNewsBadge,
  setTaskBadge,
  checkNotification,
} from "../../store/notificationReducer/actions";
import EmptyView from "../../components/EmptyView";
import PushNotificationIOS from "@react-native-community/push-notification-ios";
var PushNotification = require("react-native-push-notification");

class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loaderMoreButtonVisible: true,
      index: 1,
      refreshing: false,
      appState: AppState.currentState,
    };
  }
  componentDidMount() {
    this.checkInitial();
    this.setState({ index: 1 });
    // AppState.addEventListener("change", this._handleAppStateChange);
    this.listener = this.props.navigation.addListener("focus", () => {
      this.getNotifications(1);
    });
  }
  componentWillUnmount() {
    // AppState.removeEventListener("change", this._handleAppStateChange);
    this.listener();
  }

  checkInitial() {
    messaging()
      .getInitialNotification()
      .then((remoteMessage) => {
        if (remoteMessage != undefined) {
          this.props.checkNotification(remoteMessage);
        } else {
          // this.props.setInitialNotificaiton(undefined);
        }
      });
  }
  _handleAppStateChange = (nextAppState) => {
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === "active"
    ) {
      this.setState({ index: 1 });
      this.getNotifications(1);
    }
    this.setState({ appState: nextAppState });
  };
  getNotifications(index) {
    this.props.getNotifications(index).then((resp) => {
      this.setState({ index: index + 1 });
      this.props.setBadge(resp.tasks_unread_count + resp.news_unread_count);
      this.props.setNewsBadge(resp.news_unread_count);
      this.props.setTaskBadge(resp.tasks_unread_count);
      this.requestPermission();
      if (resp.current_page >= resp.last_page) {
        this.setState({ loaderMoreButtonVisible: false });
      }
    });
  }
  async requestPermission() {
    const granted = await messaging().requestPermission();
    if (granted) {
      this.props.registerAppWithFCM();
      this.props.registerByToken();
    } else {
    }
  }

  getNotificationDetails(id) {
    this.props.getNotificationDetails(id).then((resp) => {
      this.props.setReadedMessage(id);
      this.navigateToDetails(resp);
    });
  }

  navigateToDetails(notification) {
    this.props.navigation.navigate("NewsDetails", {
      notification: notification,
    });
  }

  getListFooter() {
    if (this.state.loaderMoreButtonVisible) {
      return (
        <TouchableOpacity
          style={styles.footerContent}
          onPress={() => {
            this.getNotifications(this.state.index);
          }}
        >
          <LabelBold style={styles.footerButton}>Załaduj więcej</LabelBold>
        </TouchableOpacity>
      );
    } else {
      return <View></View>;
    }
  }

  getRefreshControl() {
    return (
      <RefreshControl
        refreshing={this.state.refreshing}
        onRefresh={() => {
          this.getNotifications(1);
        }}
      />
    );
  }

  render() {
    return (
      <Background scrollView={false}>
        <Spacing></Spacing>
        <LabelBold style={Style.title}>Aktualności</LabelBold>
        <CView style={Style.container}>
          <FlatList
            ListEmptyComponent={<EmptyView></EmptyView>}
            contentContainerStyle={{ flexGrow: 1 }}
            refreshControl={this.getRefreshControl()}
            keyExtractor={(item, index) => index.toString()}
            data={this.props.notificationReducer.notifications}
            showsHorizontalScrollIndicator={false}
            style={styles.flexContainer}
            scrollEventThrottle={100}
            onScroll={(scroll) => {}}
            ListFooterComponent={this.getListFooter()}
            onContentSizeChange={(contentWidth) => {}}
            renderItem={({ item, index }) => (
              <NotificationItem
                key={index}
                item={item}
                onPress={() => {
                  this.getNotificationDetails(item.id);
                }}
              ></NotificationItem>
            )}
          ></FlatList>
        </CView>
        <Spacing></Spacing>
        <Spacing></Spacing>
        <Spacing></Spacing>
        <Spacing></Spacing>
      </Background>
    );
  }
}
const styles = StyleSheet.create({
  flexContainer: {
    flex: 1,
  },
  footerButton: {
    color: AppConstants.blueColor,
  },
  footerContent: { justifyContent: "center", alignItems: "center" },
});

const mapStateToProps = (state) => {
  const { appReducer, notificationReducer } = state;
  return { appReducer, notificationReducer };
};
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      registerAppWithFCM,
      registerByToken,
      getNotifications,
      getNotificationDetails,
      setReadedMessage,
      setBadge,
      setNewsBadge,
      setTaskBadge,
      checkNotification,
    },
    dispatch
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);

import React, { Component } from "react";
import { StyleSheet, View, Keyboard, Text, Image } from "react-native";
import { Background } from "../../components/Background";
import { ButtonOutline } from "../../components/ButtonOutline";
import Input from "../../components/Input";
import LabelBold from "../../components/LabelBold";
import Spacing from "../../components/Spacing";
import Style from "../../styles/styles";
import * as AppConstants from "../../constants";
import { connect } from "react-redux";
import {
  setLoggedIn,
  showLoader,
  getUserInfo,
} from "../../store/appReducer/actions";
import { showAlert, setMessage } from "../../store/alertReducer/actions";
import { login } from "../../store/sessionReducer/actions";
import { bindActionCreators } from "redux";
import { ministryLogo, logo } from "../../assets/imagesList";

class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      login: "",
      password: "",
    };
    this.loginInput = React.createRef();
    this.passwordInput = React.createRef();
  }
  componentDidMount() {}

  buttonPressed() {
    console.log(this);
    if (this.state.login.length >= 4 && this.state.password.length > 0) {
      this.props
        .login({ login: this.state.login, password: this.state.password })
        .then((resp) => {
          this.props.setLoggedIn(true);
        });
    } else {
      this.props.setMessage("Wypełnij poprawnie dane logowania.");
      this.props.showAlert(true);
    }
  }
  render() {
    return (
      <Background
        scrollView={true}
        style={{ minHeight: AppConstants.deviceHeight }}
      >
        <View style={styles.container}>
          <Image style={styles.logoImage} source={logo}></Image>
          <LabelBold style={Style.title}>Logowanie</LabelBold>
          <Input
            onUpdate={(val) => {
              this.setState({ login: val });
            }}
            placeholder="Login"
            ref={this.loginInput}
            onReturnPress={() => {
              this.passwordInput.current.setFocus();
            }}
          ></Input>
          <Spacing></Spacing>
          <Input
            onUpdate={(val) => {
              this.setState({ password: val });
            }}
            placeholder="Hasło"
            secureTextEntry={true}
            onReturnPress={() => {
              this.buttonPressed();
            }}
            ref={this.passwordInput}
          ></Input>
          <Spacing></Spacing>
          <Spacing></Spacing>
          <Spacing></Spacing>
          <Spacing></Spacing>
          <ButtonOutline
            title="Zaloguj się"
            onPress={() => {
              this.buttonPressed();
            }}
          ></ButtonOutline>
        </View>
        <View style={styles.footer}>
          <Image style={styles.footerImage} source={ministryLogo}></Image>
        </View>
      </Background>
    );
  }
}

const styles = StyleSheet.create({
  logoImage: {
    height: 100,
    maxWidth: AppConstants.deviceWidth * 0.7,
    resizeMode: "contain",
  },
  container: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  footerImage: {
    resizeMode: "contain",
    height: 80,
    width: "100%",
  },
  footer: {
    height: 100,
    paddingBottom: 10,
    width: "100%",
    backgroundColor: "#fff",
  },
});

const mapStateToProps = (state) => {
  const { appReducer } = state;
  return { appReducer };
};
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      showLoader,
      setLoggedIn,
      showAlert,
      setMessage,
      login,
      getUserInfo,
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);

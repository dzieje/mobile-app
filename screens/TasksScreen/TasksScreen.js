import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Keyboard,
  Text,
  Image,
  Button,
  RefreshControl,
  AppState,
} from "react-native";
import { Background } from "../../components/Background";
import LabelBold from "../../components/LabelBold";
import Spacing from "../../components/Spacing";
import Style from "../../styles/styles";
import * as AppConstants from "../../constants";
import CView from "../../components/CView";
import { FlatList, TouchableOpacity } from "react-native-gesture-handler";
import NotificationItem from "../../components/NotificationItem";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import messaging from "@react-native-firebase/messaging";
import {
  registerAppWithFCM,
  registerByToken,
  getTasks,
  setReadedTasks,
  getTasksDetails,
  setBadge,
  setTaskBadge,
  setNewsBadge,
} from "../../store/notificationReducer/actions";
import EmptyView from "../../components/EmptyView";
import PushNotificationIOS from "@react-native-community/push-notification-ios";
var PushNotification = require("react-native-push-notification");

class TasksScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loaderMoreButtonVisible: true,
      index: 1,
      refreshing: false,
      appState: AppState.currentState,
    };
  }
  componentDidMount() {
    this.listener = this.props.navigation.addListener("focus", () => {
      this.getTasks();
    });
  }
  componentWillUnmount() {
    // AppState.removeEventListener("change", this._handleAppStateChange);
    this.listener();
  }

  getTasks(index) {
    this.props.getTasks(index).then((resp) => {
      this.props.setBadge(resp.tasks_unread_count + resp.news_unread_count);
      this.props.setTaskBadge(resp.tasks_unread_count);
      this.props.setNewsBadge(resp.news_unread_count);
      if (resp.current_page >= resp.last_page) {
        this.setState({ loaderMoreButtonVisible: false });
      }
    });
  }

  getTasksDetails(id) {
    this.props.getTasksDetails(id).then((resp) => {
      this.props.setReadedTasks(id);
      this.navigateToDetails(resp);
    });
  }

  navigateToDetails(task) {
    this.props.navigation.navigate("TasksDetails", {
      notification: task,
      type: "task",
    });
  }

  getListFooter() {
    if (this.state.loaderMoreButtonVisible) {
      return (
        <TouchableOpacity
          style={styles.footerContent}
          onPress={() => {
            this.getTasks(this.state.index);
          }}
        >
          <LabelBold style={styles.footerButton}>Załaduj więcej</LabelBold>
        </TouchableOpacity>
      );
    } else {
      return <View></View>;
    }
  }

  getRefreshControl() {
    return (
      <RefreshControl
        refreshing={this.state.refreshing}
        onRefresh={() => {
          this.getTasks(1);
        }}
      />
    );
  }

  render() {
    return (
      <Background scrollView={false}>
        <Spacing></Spacing>
        <LabelBold style={Style.title}>Zadania</LabelBold>
        <CView style={Style.container}>
          <FlatList
            ListEmptyComponent={<EmptyView title={"Brak zadań"}></EmptyView>}
            contentContainerStyle={{ flexGrow: 1 }}
            refreshControl={this.getRefreshControl()}
            keyExtractor={(item, index) => index.toString()}
            data={this.props.notificationReducer.tasks}
            showsHorizontalScrollIndicator={false}
            style={styles.flexContainer}
            scrollEventThrottle={100}
            onScroll={(scroll) => {}}
            ListFooterComponent={this.getListFooter()}
            onContentSizeChange={(contentWidth) => {}}
            renderItem={({ item, index }) => (
              <NotificationItem
                key={index}
                item={item}
                onPress={() => {
                  this.getTasksDetails(item.id);
                }}
              ></NotificationItem>
            )}
          ></FlatList>
        </CView>
        <Spacing></Spacing>
        <Spacing></Spacing>
        <Spacing></Spacing>
        <Spacing></Spacing>
      </Background>
    );
  }
}
const styles = StyleSheet.create({
  flexContainer: {
    flex: 1,
  },
  footerButton: {
    color: AppConstants.blueColor,
  },
  footerContent: { justifyContent: "center", alignItems: "center" },
});

const mapStateToProps = (state) => {
  const { appReducer, notificationReducer } = state;
  return { appReducer, notificationReducer };
};
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      registerAppWithFCM,
      registerByToken,
      getTasks,
      setReadedTasks,
      setBadge,
      getTasksDetails,
      setTaskBadge,
      setNewsBadge,
    },
    dispatch
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(TasksScreen);

import React, { Component } from "react";
import { StyleSheet, View, Keyboard, Text, Image } from "react-native";
import { Background } from "../../components/Background";
import { ButtonOutline } from "../../components/ButtonOutline";
import Input from "../../components/Input";
import LabelBold from "../../components/LabelBold";
import Spacing from "../../components/Spacing";
import Style from "../../styles/styles";
import * as AppConstants from "../../constants";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import CView from "../../components/CView";
import { LocaleConfig } from "react-native-calendars";

import { CalendarList } from "react-native-calendars";
import { FlatList } from "react-native-gesture-handler";
import NotificationItem from "../../components/NotificationItem";

class CalendarScreen extends Component {
  constructor(props) {
    super(props);
    LocaleConfig.locales["pl"] = {
      monthNames: [
        "Styczeń",
        "Luty",
        "Marzec",
        "Kwiecień",
        "Maj",
        "Czerwiec",
        "Lipiec",
        "Sierpień",
        "Wrzesień",
        "Październik",
        "Listopad",
        "Grudzień",
      ],
      monthNamesShort: [
        "Sty.",
        "Lut.",
        "Mar",
        "Kwie",
        "Maj",
        "Cze",
        "Lip.",
        "Sie",
        "Wrz.",
        "Paź.",
        "Lis.",
        "Gru.",
      ],
      dayNames: [
        "Niedziela",
        "Poniedziałek",
        "Wtorek",
        "Środa",
        "Czwartek",
        "Piątek",
        "Sobota",
      ],
      dayNamesShort: ["Nd.", "Pon.", "Wt.", "Śr.", "Czw.", "Pt.", "Sob."],
      today: "Dziś",
    };
    LocaleConfig.defaultLocale = "pl";
  }
  items = [
    {
      title:
        "  Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      content:
        "Lorem Ipsum is simply dummy text of the printing and typesettingindustry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      unread: false,
      date: "21.10.20",
      author: "Teresa Testowa",
      photo: "",
    },
    {
      title:
        "  Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      content:
        "Lorem Ipsum is simply dummy text of the printing and typesettingindustry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      unread: true,
      date: "23.10.20",
      author: "Teresa Testowa",
      photo: "",
    },
    {
      title:
        "  Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      content:
        "Lorem Ipsum is simply dummy text of the printing and typesettingindustry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      unread: true,
      date: "24.10.20",
      author: "Jan Testowy",
      photo: "",
    },
  ];
  componentDidMount() {}
  theme = {
    todayTextColor: "black",
    selectedDayTextColor: "white",
    dayTextColor: "black",
    textSectionTitleColor: AppConstants.greyColor,
    textDisabledColor: AppConstants.greyColor,
    dotColor: AppConstants.purpleColor,
    monthTextColor: "black",
    textDayFontSize: AppConstants.contentFontSize,
    textMonthFontSize: AppConstants.contentFontSize,
    textDayHeaderFontSize: AppConstants.smallFontSize,
    textDayFontFamily: "Nunito-Regular",
    textMonthFontFamily: "Nunito-Bold",
    textDayHeaderFontFamily: "Nunito-Regular",
  };

  getCalendarView() {
    return (
      <View style={styles.calendarContainer}>
        <CalendarList
          onVisibleMonthsChange={(data) => {}}
          firstDay={1}
          horizontal
          pagingEnabled={true}
          onDayPress={(day) => {}}
          style={styles.calendarList}
          hideExtraDays={false}
          theme={this.theme}
        />
      </View>
    );
  }
  render() {
    return (
      <Background scrollView={false}>
        <Spacing></Spacing>
        <LabelBold style={Style.title}>Kalendarz</LabelBold>
        <CView style={Style.container}>
          <FlatList
            ListHeaderComponent={this.getCalendarView()}
            keyExtractor={(item, index) => index.toString()}
            data={this.items}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            style={styles.flexContainer}
            scrollEventThrottle={100}
            onScroll={(scroll) => {}}
            onContentSizeChange={(contentWidth) => {}}
            renderItem={({ item, index }) => (
              <NotificationItem key={index} item={item}></NotificationItem>
            )}
          ></FlatList>
        </CView>
        <Spacing></Spacing>
        <Spacing></Spacing>
        <Spacing></Spacing>
        <Spacing></Spacing>
      </Background>
    );
  }
}

const styles = StyleSheet.create({
  flatListContainer: {
    flex: 1,
  },
  calendarList: { width: AppConstants.deviceWidth },
  calendarContainer: {
    overflow: "hidden",
    width: AppConstants.deviceWidth * 0.9,
    justifyContent: "center",
    alignItems: "center",
    height: 350,
  },
});

const mapStateToProps = (state) => {
  const { appReducer } = state;
  return { appReducer };
};
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({}, dispatch);
};
export default connect(mapStateToProps, mapDispatchToProps)(CalendarScreen);

import { StyleSheet, View, Keyboard, Text, Platform } from "react-native";
import * as AppConstants from "../constants";

export default Style = StyleSheet.create({
  text: {
    fontSize: 15,
  },
  title: {
    fontSize: 20,
  },

  shadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.5,
    shadowRadius: 8,

    elevation: Platform.OS == "ios" ? 6 : 0,
  },
  container: {
    flex: 1,
    width: AppConstants.deviceWidth * 0.9,
  },
});

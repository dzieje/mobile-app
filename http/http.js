import axios from "axios";
import store from "../store/index";

import {
  setLoader,
  showLoader,
  setLoggedIn,
} from "../store/appReducer/actions";
import { showAlert, setMessage } from "../store/alertReducer/actions";
import * as AppConstants from "../constants";

const timeout = 10000;
axios.defaults.baseURL = AppConstants.URL;
axios.defaults.timeout = timeout;
axios.interceptors.request.use((config) => {
  //Answer on questions without loader
  if (
    config.url.indexOf("api-mobile/registration/") != -1 ||
    config.url.indexOf("api-mobile/password/reset") != -1
  ) {
  } else {
    config.headers["Authorization"] = `Bearer ${
      store.getState().sessionReducer.accessToken
    }`;
  }
  showLoader(true);

  console.log(config);
  return config;
});
const responeInterceptors = axios.interceptors.response.use(
  (response) => {
    showLoader(false);
    return Promise.resolve(response);
  },
  (error) => {
    showLoader(false);
    var show = true;
    var message = "Wystąpił problem podczas próby połączenia z serwerem";

    if (error.code == "ECONNABORTED") {
      store.dispatch(setMessage(message));

      return Promise.reject({ response: { data: { code: "timeout" } } });
    }
    if (error.response) {
      if (
        error.response.status == 401 &&
        store.getState().sessionReducer.loginRequestRepeated == true
      ) {
        message = "Proszę zalogować się ponownie";
        store.dispatch(setLoggedIn(false));
      } else if (
        error.response.status == 401 &&
        store.getState().sessionReducer.loginRequestRepeated == false
      ) {
        store.dispatch(setLoginRequestRepated(true));

        return renewLoginRequest(error);
      }
      if (error.response.data) {
        if (error.response.data.message) {
          message = error.response.data.message;
        } else if (error.response.data.error == "invalid_credentials") {
          message = "Podane dane są nieprawidłowe";
        } else if (error.response.data.error == "invalid_request") {
          message = "";
          show = false;
        }
      }
    } else {
      var message = "Wystąpił problem podczas próby połączenia z serwerem";
    }

    store.dispatch(setMessage(message));
    if (store.getState().alertReducer.disable == false) {
      store.dispatch(showAlert(show));
    }
    return Promise.reject(returnError(error));
  }
);

function renewRequest(request) {
  if (request.method == "get") {
    return axios
      .get(request.url, {})
      .then(function (response) {
        return Promise.resolve(response);
      })
      .catch(function (error) {
        return Promise.reject(returnError(error));
      });
  } else if (request.method == "post") {
    data = request.data ? JSON.parse(request.data) : {};
    return axios
      .post(request.url, data, {})
      .then(function (response) {
        return Promise.resolve(response);
      })
      .catch(function (error) {
        return Promise.reject(returnError(error));
      });
  }
}

function renewLoginRequest(error) {
  const instance = axios.create();
  return SecureStore.getItemAsync(AppConstants.refreshTokenName).then(
    (token) => {
      return instance
        .post("oauth/token", {
          grant_type: "refresh_token",
          refresh_token: token,
          client_id: AppConstants.clientID,
          client_secret: AppConstants.clientSecret,
          scope: "*",
        })
        .then(function (resp) {
          store.dispatch(setAccessToken(resp.data.access_token));
          return SecureStore.setItemAsync(
            AppConstants.refreshTokenName,
            resp.data.refresh_token
          ).then((ssResponse) => {
            return renewRequest(error.config);
          });
        })
        .catch(function (err) {
          store.dispatch(backToLogin());
          return Promise.reject(returnError(error));
        });
    }
  );
}

function returnError(error) {
  if (error.response) {
    if (error.response.data) {
      return error;
    } else {
      return { response: { data: { code: "timeout" } } };
    }
  } else {
    return { response: { data: { code: "timeout" } } };
  }
}
